<?php
include "header.php";


include "db_connection.php";
$sql = "SELECT model.*,manufacturer.name FROM model LEFT JOIN manufacturer ON manufacturer.id = model.m_id";

$result = $conn->query($sql);

?>
<html>
	<head>
		<script src = "https://code.jquery.com/jquery-3.3.1.js"></script>
		<script src = "https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
		<link href = "https://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css" rel = "stylesheet">
	
	</head>
	
	<body>
	<table id="example" class="display" style="width:100%">
        <thead>
            <tr>
	<th>ID</th>
    <th>Manufacturer name</th>
    <th>Model Name</th>
    <th>Manufacturing year</th>
    <th>Colour</th>
    <th>Quantity</th>
    <th>price</th>
    <th>Description</th>	
	<th>Action</th>
	
</tr>
        </thead>
		<?php foreach ($result as $item){ ?>
<tr>
     <td><?php echo $item['id'] ?></td>
     <td><?php echo $item['name'] ?></td>
     <td><?php echo $item['model_name'] ?></td>
	 <td><?php echo $item['manufacturing_year'] ?></td>
	 <td><?php echo $item['colour'] ?></td>
	 <td><?php echo $item['quantity'] ?></td>
	 <td><?php echo $item['price'] ?></td> 
	 <td><?php echo $item['description'] ?></td> 
	 
	 
	<td><a href="car_edit.php?id=<?php echo $item['id'] ?>">Edit </a>&nbsp;
	 <a href="car_delete.php?id=<?php echo $item['id'] ?>">Delete</td>	 
</tr>
<?php } ?>
<?php

$conn->close();
?>
        <tfoot>
            <tr>
	<th>ID</th>
    <th>Manufacturer name</th>
    <th>Model Name</th>
    <th>Manufacturing year</th>
    <th>Colour</th>
    <th>Quantity</th>
    <th>price</th>
    <th>Description</th>
	<th>Action</th>
	
</tr>

        </tfoot>
    </table>
	</body>
	<script>
		$(document).ready(function() {
    $('#example').DataTable( {
        
    } );
} );
	</script>
</html>