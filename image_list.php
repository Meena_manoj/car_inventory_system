<?php
include "header.php";

include "db_connection.php";

$sql = "SELECT
    test.id,
	model.model_name,
    manufacturer.name,
    test.image
FROM
    test
LEFT JOIN model ON test.car_id = model.id
LEFT JOIN manufacturer ON manufacturer.id = model.m_id GROUP BY manufacturer.name";

$result = $conn->query($sql);

?>
<html>
	<head>
		<script src = "https://code.jquery.com/jquery-3.3.1.js"></script>
		<script src = "https://cdn.datatables.net/1.10.18/js/jquery.dataTables.min.js"></script>
		<link href = "https://cdn.datatables.net/1.10.18/css/jquery.dataTables.min.css" rel = "stylesheet">
	
	</head>
	
	<body>
	<table id="example" class="display" style="width:100%">
        <thead>
            <tr>
	<th>ID</th>
    <th>Manufacturer name</th>
    <th>Model Name</th>
    <th>Image</th>
	<th>Action</th>
	
</tr>
        </thead>
		<?php foreach ($result as $item){ ?>
<tr>
     <td><?php echo $item['id'] ?></td>
     <td><?php echo $item['name'] ?></td>
     <td><?php echo $item['model_name'] ?></td>
     <td><?php echo "<img src = 'images/" .$item['image']."' >"; ?></td>
	 
	<td><a href=".php?id=<?php echo $item['id'] ?>">Edit </a>&nbsp;
	 <a href=".php?id=<?php echo $item['id'] ?>">Delete</td>	 
</tr>
<?php } ?>
<?php

$conn->close();
?>
        <tfoot>
            <tr>
	<th>ID</th>
    <th>Manufacturer name</th>
    <th>Model Name</th>
    <th>Image</th>
	<th>Action</th>
    
</tr>

        </tfoot>
    </table>
	</body>
	<script>
		$(document).ready(function() {
    $('#example').DataTable( {
        
    } );
} );
	</script>
</html>